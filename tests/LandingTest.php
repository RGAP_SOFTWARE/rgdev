<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LandingTest extends TestCase
{
    /**
     * Insure that main page was loaded
     *
     * @return void
     */
    public function testLoadMainPage()
    {
        $this->visit('/landing')
        	 ->see('<title>RG | Landing</title>');
    }

    /**
		* insure that we got all data for our landing page
		*
		*
     * @return void
     */

    public function testPageJson()
    {
    	$this->get('/landing/get')
    	 	 ->seeJsonStructure(['data', 'info1', 'info2']);
    }


    /**
	* insure that 
    *
     * @return void
     */
}

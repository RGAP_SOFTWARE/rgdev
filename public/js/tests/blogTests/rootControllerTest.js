describe("root controller tests", function () {

    var mockScope = {};
    var controller;
    beforeEach(angular.mock.module('blogApp'));

    beforeEach(angular.mock.inject(function ($controller, $rootScope) {

        mockScope = $rootScope.$new();

        controller = $controller('rootController', {
            $scope: mockScope
        });

    }));

    it("init variables", function () {
        expect(mockScope.loading).toBe(true);
        expect(mockScope.creatingSuccess).toBe(false);
        expect(mockScope.deletingSuccess).toBe(false);
        expect(mockScope.updatingSuccess).toBe(false);
    });
    it('execute functions', function () {
        mockScope.hideNotifications();
        expect(mockScope.creatingSuccess).toBe(false);
        expect(mockScope.deletingSuccess).toBe(false);
        expect(mockScope.updatingSuccess).toBe(false);
    });
});
blogApp.directive('validFile',function(){
	  return {
	    require:'ngModel',
	    link:function(scope,el,attrs,ngModel){
	      //change event is fired when file is selected
	      el.bind('change',function(){
	        scope.$apply(function(){
	          ngModel.$setViewValue(el.val());
	          ngModel.$render();
	        });
	      });
	    }
	  }
	});

blogApp.directive('useDefaultValue', function ($parse) {
	    return {
	      link: function (scope, element, attrs) {
	        if (attrs.ngModel && element[0].defaultValue) {
	          $parse(attrs.ngModel).assign(scope, element[0].defaultValue);
	        }
	      }
	    };
	});

blogApp.directive('fileModel', function () {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
	        element.bind('change', function(event){
            	if(attrs.multiple === true){
	            	var len = event.target.files.length;
	            	for(var i = 0; i < len; i++){
		                scope.$apply(function(){
		                    scope[attrs.fileModel].push(element[0].files[i]);
		                });
	            	}
	            }else{
	            	scope.$apply(function(){
		                scope[attrs.fileModel][0] = element[0].files[0];
		            });
	            }
	        });

        }
    };
});

blogApp.directive('passwordConfirmation', function(){
	return {
		require: 'ngModel',
		restrict: 'A',
		scope: {
			password: '=passwordConfirmation'
		},
		link: function(scope, element, attrs, ngModel){
			ngModel.$validators.passwordConfirmation = function(modelValue) {
				return modelValue == scope.password;
			};

			scope.$watch("password", function() {
				ngModel.$validate();
			});
		}
	}
});

blogApp.directive('windowSize', ['$window', '$rootScope', function($window, $rootScope){
	return {
		restrict: 'A',
		link: function(){
			$rootScope.windowWidth = $window.innerWidth;
			$rootScope.windowHeight = $window.innerHeight;

			angular.element($window).bind('resize', function(){
				$rootScope.$apply(function(){
					$rootScope.windowWidth = $window.innerWidth;
					$rootScope.windowHeight = $window.innerHeight;
				});
			});
		}
	}
}]);
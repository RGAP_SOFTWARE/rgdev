	/**
     * Service for articles
     *
     * @param  http ajax servise, promise service
     * @return 
    */

blogApp.service('article', ['$http', '$q', function($http, $q){
	
	var article = this;
	article.blogData = {};

	// delete a comment

	article.deleteComment = function(id){
		var defer = $q.defer();

		$http.delete('/delete/comment/' + id).success(function(res){
			defer.resolve(res);
		}).error(function(res){
			defer.reject(res);
		});

		return defer.promise;
	}

    //save or edit comment

    article.saveComment = function(data, token, mode){
        var defer = $q.defer();

        var formData = new FormData();

		try{
            for(var key in data)
                formData.append(key, data[key]);
        }catch(e){
            console.log(e.message);
        }

		var url = '/save/comment';

		if(mode === 'edit')
			url = '/edit/comment';

        $http.post(url, formData,{
            transformRequest: angular.indentity,
            headers: { 'Content-Type': undefined, 'X-CSRF-TOKEN': token}
        }).
        success(function (data) {
            defer.resolve(data);
        }).
        error(function (data) {
            defer.reject(data);
        });

        return defer.promise;

    };

	// get comments
	article.getComments = function(id){
		var defer = $q.defer();

		$http.get('/blog/article/comments/' + id)
			 .success(function(res){
				 defer.resolve(res);
			 })
			.error(function(err){
				defer.reject(err);
			});

		return defer.promise;
	};

    // get single article
    article.getArticle = function(id){
        var defer = $q.defer();

        $http.get('/blog/article/' + id).success(function(res){
            defer.resolve(res);
            article.blogData.data = {
                0: res.article
            };
            article.blogData.tags = res.tags;
        }).error(function(res){
           defer.reject(res);
        });

        return defer.promise;
    };

		/**
	     * get all articles from data base
	     *
	     * @param  quantity of articles, sorting ('desc, asc')
	     * @return articles json
	    */

	article.getAll = function(quantity, order, mode, query, page){
		var defer = $q.defer();

		page = page || 1;

		quantity = quantity || 3;

		order = order || 'desc';

		$http.get('/blog/articles/' + quantity + '/' + order + '/' + mode + '/' + query + '/?page=' + page)
			 .success(function(res){
			 	article.blogData = res;
			 	defer.resolve(res);
			 })
			 .error(function(err, status){
			 	defer.reject(err)
			 });

		return defer.promise;
	};

		/**
	     * create an article
	     *
	     * @param  article object
	     * @return 
	    */

	article.articleFormHandler = function(data, token, mode){
		var defer = $q.defer();
		var url = "/blog/create";
		var formData = new FormData();

	    for(var key in data.model)
	      	formData.append(key, data.model[key]);

	    try{
		    for(var key in data.files)
		      	formData.append('file_' + key, data.files[key]);
	    }catch(e){
	    	console.log(e.message);
	    }

	    if(mode === 'update')
	    	url = "/blog/update";

		$http.post(url, formData,{
	            transformRequest: angular.indentity,
	            headers: { 'Content-Type': undefined, 'X-CSRF-TOKEN': token}
	    }).
	    success(function (data, status, headers, config) {
	        defer.resolve(data);
	    }).
	    error(function (data, status, headers, config) {
	        defer.reject(data);
	    });

		return defer.promise;
	};


		/**
	     * delete an article
	     *
	     * @param  id of article, user id
	     * @return 
	    */

	article.delete = function(id, user_id){
		var defer = $q.defer();

		$http.delete('/blog/delete/' + id + '/' + user_id)
			 .success(function(res){
			 	defer.resolve(res);
			 })
			 .error(function(err, status){
			 	defer.reject(err);
			 });

		return defer.promise;
	};

	return article;

}]);

blogApp.service('auth', ['$http', '$q', function($http, $q){

	var auth = this;

	auth.currentUser = null;

	auth.check = function(){
		var defer = $q.defer();

		$http.get('/get/user').success(function(res){
			defer.resolve(res);
		}).error(function(err){
            defer.reject(err);
		});

		return defer.promise;
	};

	auth.autharization = function(data, token, mode){
		var defer = $q.defer();

		var formData = new FormData();

		try{
			for(var key in data)
				formData.append(key, data[key]);
		}catch(e){
			console.log(e.message);
		}

		var url = '';

		switch(mode){
			case 'login':
				url = '/login';
				break;
			case 'register':
				url = '/register';
				break;
			case 'reset':
				url = '/reset';
				break;
			case 'logout':
				url = '/logout';
				break;
		}

		if(mode !== 'logout') {
			$http.post(url, formData, {
				transformRequest: angular.indentity,
				headers: {'Content-Type': undefined, 'X-CSRF-TOKEN': token}
			}).success(function (data) {
				defer.resolve(data);
			}).error(function (data) {
				defer.reject(data);
			});
		}else{
			$http.get(url).success(function (data) {
				defer.resolve(data);
			}).error(function (data) {
				defer.reject(data);
			});
		}

		return defer.promise;
	}

}]);
blogApp.filter('flUpper', function() {
  return function(input, scope) {
    if (input !== null && typeof input === "string")
    input = input.toLowerCase();
    return input.substring(0,1).toUpperCase()+input.substring(1);
  }
});
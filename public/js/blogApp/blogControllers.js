
blogApp.controller('rootController', ['$scope', '$rootScope', 'article', '$uibModal', 'auth', function($scope, $rootScope, article, $uibModal, auth){


	$rootScope.photoPathDetector = function(){
		if($rootScope.windowWidth >= 1200){
			$rootScope.photoPath = 'lg';
		}else if($rootScope.windowWidth < 1200 && $rootScope.windowWidth >= 978){
			$rootScope.photoPath = 'md';
		}else if($rootScope.windowWidth < 978 && $rootScope.windowWidth >= 768){
			$rootScope.photoPath = 'sm';
		} else {
			$rootScope.photoPath = 'xs';
		}
	};

	$rootScope.$watch('windowWidth', function(){
		$rootScope.photoPathDetector();
	});
		/**
	     * notification params
	     *
	    */

	$scope.loading = true;

	$scope.creatingSuccess = false;
	$scope.deletingSuccess = false;
	$scope.updatingSuccess = false;
    $scope.loginSuccess = false;
    $scope.registerSuccess = false;
    $scope.resetSuccess = false;
    $scope.logoutSuccess = false;
    $scope.userNotExists = false;

	$scope.authDetector = false;

	/**
     * events for notifications 
     *
    */

	$rootScope.$on('loading', function(event, val){
		$scope.loading = val;
	});

	$rootScope.$on('successfullyCreated', function(event){
		$scope.creatingSuccess = true;
	});

	$rootScope.$on('successfullyDeleted', function(event){
		$scope.deletingSuccess = true;
	});

	$rootScope.$on('successfullyUpdated', function(event){
		$scope.updatingSuccess = true;
	});

    $rootScope.$on('successfullyLoggedin', function(event){
        $scope.loginSuccess = true;
    });

    $rootScope.$on('successfullyRegistered', function(event){
        $scope.registerSuccess = true;
    });

    $rootScope.$on('successfullyResets', function(event){
        $scope.resetSuccess = true;
    });

    $rootScope.$on('successfullyLoggedout', function(event){
        $scope.logoutSuccess = true;
    });

    $rootScope.$on('switchAuthDetector', function(a,b){
        $scope.authDetector = b;
    });

    $rootScope.$on('userDoesNotExists', function(a,b){
        $scope.userNotExists = b;
    });

	/**
     * hide notifications
     *
    */

    $scope.hideNotifications = function(){
		$scope.creatingSuccess = false;
		$scope.deletingSuccess = false;    	
		$scope.updatingSuccess = false;
        $scope.loginSuccess = false;
        $scope.registerSuccess = false;
        $scope.resetSuccess = false;
        $scope.logoutSuccess = false;
        $scope.userNotExists = false;
    };

    $scope.logout = function($event, mode){
        $event.preventDefault();

        var data = {};

        mode = 'logout';

        var token = '';

        auth.autharization(data,token,mode).then(function(){

             $rootScope.$broadcast('switchAuthDetector', false);
             $rootScope.$broadcast('refresh', mode);
             $rootScope.$broadcast('articleReload', mode);

         },function(res){
                console.log(res);
                $uibModalInstance.close();
        });

    };

    $scope.deleteModal = function(id, user_id, point){
		var modalInstance = $uibModal.open({
			templateUrl: '/view/blogView/deleteModal.htm',
			controller: 'deleteModalController',
			animation: false
		});

		var place = point || 'home';

		modalInstance.article = {
			id: id,
			user_id: user_id,
			place: place
		}

	};

	// autharization modal

	$scope.authModal = function($event){
		$event.preventDefault();

		var modalInstance = $uibModal.open({
			templateUrl: '/view/blogView/authModal.htm',
			controller: 'authController',
			animation: false
		});

        modalInstance.modalTitle = 'Log in';
	};

	/**
	     *
	     *
	    */

	$scope.articleFormModal = function($event, action, index, fromArticle){

		var triger = action || "create";

		var index = index || 0;

        var fromArticle = fromArticle || 0;

		$event.preventDefault();

		var modalInstance = $uibModal.open({
			templateUrl: '/view/blogView/blogArticleForm.htm',
			controller: 'articleFormController',
			animation: false,
			size: 'lg'
		});

		if(triger === 'update'){

			var len = article.blogData['data'][index]['tags'].length;
			var tags = [];
			for(var i = 0; i < len; i++){
				tags[i] = article.blogData['data'][index]['tags'][i]['id'];
			}

			modalInstance.article = {
				title: article.blogData['data'][index]['title'],
				body:  article.blogData['data'][index]['body'],
				tags:  tags,
				user_id:  article.blogData['data'][index]['user_id'],
				id:  article.blogData['data'][index]['id'],
				modalTitle:  'Edit the article.'
			};

			modalInstance.mode = 'update';
			modalInstance.buttonText = 'Edit the article';
            modalInstance.fromArticle = fromArticle;

        }else{
            modalInstance.article = {
				title: '',
				body:  '',
				tags:  [],
				modalTitle: 'Create a new article.'
			};

			modalInstance.mode = 'create';
            modalInstance.buttonText = 'Create a new article';
        }

		modalInstance.result.then(function(){
			// ok
		},
		function(){
			// cancel
		});
	}


}]);

blogApp.controller('authController', ['$rootScope', '$scope', '$uibModalInstance', 'auth', 'article', function($rootScope, $scope, $uibModalInstance, auth, article){

    $scope.modalTitle = $uibModalInstance.modalTitle;
    $scope.loginLayout = true;
    $scope.signupLayout = false;
    $scope.resetLayout = false;

    $scope.userLoginData = {
      email: '',
      password: '',
	  remember: 0
    };

    $scope.userRegData = {
      name: '',
      email: '',
      password: '',
      password_confirmation: ''
    };

    $scope.userResetData = {
      email: ''
    };

    $scope.token = article.csrfToken;

    $scope.auth = function($event, mode){

		if(typeof $event === 'object')
    	  $event.preventDefault();

        $scope.disable = true;

      var data = {};
      var token = $scope.token;

        switch(mode){
            case 'login':
                data = $scope.userLoginData;
                break;
            case 'register':
                data = $scope.userRegData;
                break;
            case 'reset':
                data = $scope.reset;
                break;
        }

      auth.autharization(data,token,mode).then(function(res){
          $uibModalInstance.close();
          $scope.disable = false;

          if(mode == 'login' || mode == 'register'){
			  auth.check().then(function(res){
				  if(res == 'true'){
                      $rootScope.$broadcast('switchAuthDetector', true);
                      $rootScope.$broadcast('refresh', mode);
                      $rootScope.$broadcast('articleReload', mode);
                  }else{
                      $rootScope.$broadcast('userDoesNotExists', true);
                  }
			  });

		  }

      },function(res){
          console.log(res);
          $uibModalInstance.close();
      });
    };

    $scope.close = function(){
      $uibModalInstance.close();
    };

    $scope.switchLayout = function($event, layout){

        $event.preventDefault();

        switch(layout){
            case 'login':
                $scope.loginLayout = true;
                $scope.signupLayout = false;
                $scope.resetLayout = false;
                $scope.modalTitle = 'Log in';
                break;
            case 'signup':
                $scope.loginLayout = false;
                $scope.resetLayout = false;
                $scope.signupLayout = true;
                $scope.modalTitle = 'Sign up';
                break;
            case 'reset':
                $scope.loginLayout = false;
                $scope.signupLayout = false;
                $scope.resetLayout = true;
                $scope.modalTitle = 'Reset old password';
                break;
        }

    };

}]);

blogApp.controller('deleteModalController', ['$scope', 'article', '$uibModalInstance', '$rootScope', '$location', function($scope, article, $uibModalInstance, $rootScope, $location){

	$scope.close = function(){
		$uibModalInstance.close();
	};

		/**
	     * delete the article
	     *
	    */

	$scope.article = $uibModalInstance.article;

	$scope.deleteArticle = function(id, user_id, place){

		$uibModalInstance.close();
		$rootScope.$broadcast('loading', true);

		article.delete(id, user_id)
			   .then(function(){
			   		//get relevant articles from server
			   		if(place === 'home') {
                        $rootScope.$broadcast('refresh', 'delete');
                    }else {
                        $rootScope.$broadcast('successfullyDeleted');
                        $location.path('/');
                    }
				}, function(res){
					// success denied
				});
	}

}]);

// Header handler

blogApp.controller('headerController', ['$scope', '$uibModal', 'article', '$location', function($scope, $uibModal, article, $location){


	$scope.$watch('csrf', function(){
		article.csrfToken = $scope.csrf;
	});


	$scope.prevent = function($event){
		$event.preventDefault();
	};


	// search
	$scope.search = function($event){
		$event.preventDefault();

		$location.path('/search/' + $scope.q);
	}

}]);

//

blogApp.controller('articleFormController', ['$scope', '$uibModalInstance', 'article', '$rootScope', '$location', 'auth', function($scope, $uibModalInstance, article, $rootScope, $location, auth){


	$scope.allTags = article.blogData.tags;
	$scope.showForm = false;
	$scope.showAlert = false;

	auth.check().then(function(res){

		if(res === 'true'){
			$scope.showForm = true;
		}else{
			$scope.showAlert = true;
		}

	}, function(){
		auth.check();
	});

	console.log($scope.authFormDetector);

	$scope.close = function(){
		$uibModalInstance.close();
	};

	$scope.modalTitle = $uibModalInstance.article['modalTitle'];
	$scope.modalButtonText = $uibModalInstance.buttonText;
	$scope.formMode = $uibModalInstance.mode;

	$scope.data = {
		title: $uibModalInstance.article['title'],
		body: $uibModalInstance.article['body'],
		tags: $uibModalInstance.article['tags']
	};

	$scope.images = [];


	$scope.articleForm = function($event, mode){

		$event.preventDefault();

		$scope.disable = true;

		var data = {
			model: $scope.data,
			files: $scope.images
		};

		var token = article.csrfToken;

		if(mode === 'update'){
			data.model['user_id'] = $uibModalInstance.article['user_id'];
			data.model['id'] = $uibModalInstance.article['id'];
		}

		article.articleFormHandler(data, token, mode).then(function(){
			//success
			$uibModalInstance.close();
			$scope.disable = false;
			if($scope.formMode === 'update') {
                $rootScope.$broadcast('refresh', 'update');
            }else {

                var path = document.location.hash;
                if(path.match(/article/)){
                    $location.path('/');
                    return;
                }

                $rootScope.$broadcast('refresh', 'create');
            }

            if($uibModalInstance.fromArticle) {
                $rootScope.$broadcast('articleReload');
            }
		},
		function(){
			//failed
		})
	}

}]);

// Content handaler

blogApp.controller('articlesController', ['$scope', 'article', '$rootScope', '$routeParams', '$location', function($scope, article, $rootScope, $routeParams, $location){

	$scope.init = function(){

		//pagination settings
		$scope.pagination = {
			currentPage: '',
			totalItems: '',
			itemsPerPage: '',
			display: true
		};

		$scope.filters = {
			order: 'desc',
			quantity: 3
		};

		$scope.search = {
			mode: $routeParams.mode || 'firstTime',
			query: $routeParams.query || 'none'
		};

		$scope.noResults = false;
        $scope.manyResults = true;

		$scope.pageTitle = '';

		$scope.articles = {};

        $scope.loadPage();

	};

	// load page

	$scope.loadPage = function(reason){

		var reason = reason || false;
		var order = $scope.filters.order;
		var q = $scope.filters.quantity;
		var page = $scope.pagination.currentPage;

		$scope.getAllArticles(reason, q, order, page);
	};

	// switch order of articles

	$scope.switchOrder = function(){
		if($scope.filters.order === 'desc')
			$scope.filters.order = 'asc';
		else
			$scope.filters.order = 'desc';

		$scope.loadPage();
	};

	// events

	$scope.$on('refresh', function(event, reason){
		$scope.loadPage(reason);
	});

		/**
	     * get articles for the scope
	     *
	    */

	$scope.getAllArticles = function(reason, quantity, order, page){

		reason = reason || false;
		var mode = $scope.search.mode;
		var query = $scope.search.query;

		$rootScope.$broadcast('loading', true);

		article.getAll(quantity, order, mode, query, page).then(function(res){
			//success
			$scope.articles = res;
			//set pagination settings
			$scope.pagination.totalItems = res.total;
			$scope.pagination.itemsPerPage = res.per_page;
			$scope.subTitle = res.subTitle || '';
			$scope.filters.order = res.order;
			$scope.search.mode = $routeParams.mode;

            if(res.currentUser !== null){
                $rootScope.$broadcast('switchAuthDetector', true);
            }

            if(res.per_page >= res.total){
                $scope.pagination.display = false;
            }else{
                $scope.pagination.display = true;
            }

            if(res.data.length == 1)
                $scope.manyResults = false;
            else
                $scope.manyResults = true;

            if(res.data[0])
				$scope.noResults = false;
			else
				$scope.noResults = true;

			//set page title
			$scope.pageTitle = res.title;
			//switch off load gif
			$rootScope.$broadcast('loading', false);

			if(reason !== false){
				switch(reason){
					case 'create':
						$rootScope.$broadcast('successfullyCreated');
					    break;
					case 'delete':
						$rootScope.$broadcast('successfullyDeleted');
					    break;
					case 'update':
						$rootScope.$broadcast('successfullyUpdated');
					    break;
                    case 'login':
                        $rootScope.$broadcast('successfullyLoggedin');
                        break;
                    case 'register':
                        $rootScope.$broadcast('successfullyRegistered');
                        break;
                    case 'reset':
                        $rootScope.$broadcast('successfullyResets');
                        break;
                    case 'logout':
                        $rootScope.$broadcast('successfullyLoggedout');
                        break;
				}
			}
		},
		function(err){
			// error
            console.log(err);
			$scope.loadPage();
		});
	};

	$scope.init();

}]);

// Footer handaler


blogApp.controller('footerController', ['$scope', function($scope){



}]);

blogApp.controller('articleController', ['$scope', 'article', '$routeParams', '$rootScope', '$q', function($scope, article, $routeParams, $rootScope, $q){

    $scope.init = function(){

        $scope.article = {};
		$scope.comments = {};
        $scope.creator = false;
		$scope.commentsLoading = false;

        $scope.getData();
		$scope.getComments();

    };

	$scope.deleteTheComment = function(id){
		article.deleteComment(id).then(function(res){
			console.log(res);
			$scope.getComments();
		},function(res){
			console.log(res);
		});
	};

	$scope.editComment = function(body, comment_id, user_id){

		var data = {
			body: body,
			id: comment_id,
			user_id: user_id
		};

		var token = article.csrfToken;

		article.saveComment(data, token, 'edit').then(function(res){
			console.log(res);
		}, function(res){
			console.log(res);
		});

	};

    $scope.$on('commentsLoading', function(e,p){
       $scope.commentsLoading = p;
    });

    $scope.$on('commentsReload', function(){
       $scope.getComments();
    });

	$scope.getComments = function(){

        $scope.commentsLoading = true;

		article.getComments($routeParams.id).then(function(res){
			$scope.comments = res;
            $scope.commentsLoading = false;
		},function(){
            $scope.getComments();
        });
	};

    $scope.$on('articleReload', function(a,b){
        $rootScope.$broadcast('loading', true);
        $scope.loadArticle(b);
    });

    $scope.currentDataSearch = function(){
        if(article.blogData.data) {
            for (var index in article.blogData.data) {
                if (article.blogData.data[index].id == $routeParams.id) {
                    $scope.article = article.blogData.data[index];
                    $scope.article['currentUser'] = article.blogData.currentUser;
                    $scope.creator = $scope.article.currentUser !== null && $scope.article.currentUser.id === $scope.article.user_id;

                    return true;
                }
            }
        }
        return false;
    };

    $scope.loadArticle = function(reason){

        var reason = reason || false;

        article.getArticle($routeParams.id).then(function(res){
            $scope.article = res.article;
            $scope.creator = $scope.article.currentUser !== null && $scope.article.currentUser.id === $scope.article.user_id;
            $rootScope.$broadcast('loading', false);

            if($scope.article.currentUser)
                $rootScope.$broadcast('switchAuthDetector', true);

            if(reason !== false){
                switch(reason){
                    case 'update':
                        $rootScope.$broadcast('successfullyUpdated');
                        break;
                    case 'login':
                        $rootScope.$broadcast('successfullyLoggedin');
                        break;
                    case 'register':
                        $rootScope.$broadcast('successfullyRegistered');
                        break;
                    case 'reset':
                        $rootScope.$broadcast('successfullyResets');
                        break;
                    case 'logout':
                        $rootScope.$broadcast('successfullyLoggedout');
                        break;
                }
            }

        }, function(){
            $scope.loadArticle();
        });
    };

    $scope.getData = function(){
        $rootScope.$broadcast('loading', true);

        if($scope.currentDataSearch() === false){
            $scope.loadArticle();
        }else{
            $rootScope.$broadcast('loading', false);
        }

    };


    $scope.init();

}]);

blogApp.controller('commentController', ['$scope', 'article', '$rootScope', function($scope, article, $rootScope){

    $scope.comment = {
      body: '',
      user_id: '',
      article_id: ''
    };

    $scope.saveComment = function($event, user_id, article_id){

        $event.preventDefault();

        $scope.comment.user_id = user_id;
        $scope.comment.article_id = article_id;

        var data = $scope.comment;

        var token = article.csrfToken;

        $rootScope.$broadcast('commentsLoading', true);

        article.saveComment(data, token).then(function(res){
            console.log(res);
            $rootScope.$broadcast('commentsLoading', true);
            $rootScope.$broadcast('commentsReload');
        },function(res){
           console.log(res);
        });
    };

}]);

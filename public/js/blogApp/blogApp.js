var blogApp = angular.module("blogApp", ['ngRoute',
										'ngResource',
										'ngAnimate',
										'ngTouch',
										'ngMessages',
										'ui.select2', 
										'ui.bootstrap',
										'xeditable'])
					 .config(['$routeProvider', function($routeProvider){

					 	$routeProvider
					 		.when('/', {
					 			templateUrl: '/view/blogView/blogArticles.htm',
					 			controller: 'articlesController'
					 		})
					 		.when('/article/:id/', {
					 			templateUrl: '/view/blogView/blogArticle.htm',
					 			controller: 'articleController'
					 		})
					 		.when('/:mode/:query/',{
					 			templateUrl: '/view/blogView/blogArticles.htm',
					 			controller: 'articlesController' 
					 		})
					 		.otherwise({redirectTo: '/'});

					}]);

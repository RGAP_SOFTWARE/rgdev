landingApp.directive('mainBannerSize', ['$log', '$window', function($log, $window){

    return {
        restrict: 'A',
        scope: {},
        link: function(scope, element){

            element.css('height', $window.innerHeight);

            angular.element($window).bind('resize', function(){
                element.css('height', $window.innerHeight);
            });

        }
    };

}]);

landingApp.directive('windowSize', ['$log', '$window', '$rootScope', function($log, $window, $rootScope){

    return {
        restrict: 'A',

        link: function(scope){
            $rootScope.windowWidth = $window.innerWidth;
            $rootScope.windowHight = $window.innerHeight;

            angular.element($window).bind('resize', function(){
                scope.$apply(function(){
                   scope.windowWidth = $window.innerWidth;
                   scope.windowHight = $window.innerHeight;
                    $rootScope.windowWidth = $window.innerWidth;
                    $rootScope.windowHight = $window.innerHeight;
                });
            });
        }
    };

}]);

landingApp.directive('fileModel1', function () {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('change', function(event){
                var file = new FileReader();

                file.onload = function(e){
                    scope.$apply(function(){
                        scope.infoblock1Img[attrs.fileModel1] = e.target.result;
                        scope.infoblockFiles[1][attrs.infoId] = element[0].files[0];
                    });
                };

                file.readAsDataURL(event.target.files[0]);

            });

        }
    };
});

landingApp.directive('fileModel2', function () {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('change', function(event){
                var file = new FileReader();

                file.onload = function(e){
                    scope.$apply(function(){
                        scope.infoblock2Img[attrs.fileModel2] = e.target.result;
                        scope.infoblockFiles[2][attrs.infoId] = element[0].files[0];
                    });
                };

                file.readAsDataURL(event.target.files[0]);

            });

        }
    };
});

landingApp.directive('landingFile', function () {
    return {
        restrict: 'A',
        link: function(scope, element) {
            element.bind('change', function(event){
                var file = new FileReader();

                file.onload = function(e){
                    scope.$apply(function(){
                        scope.landingImage = e.target.result;
                        scope.landingFile = element[0].files[0];
                    });
                };

                file.readAsDataURL(event.target.files[0]);

            });

        }
    };
});

landingApp.directive('validFile',function(){
    return {
        require:'ngModel',
        link:function(scope,el,attrs,ngModel){
            //change event is fired when file is selected
            el.bind('change',function(){
                scope.$apply(function(){
                    ngModel.$setViewValue(el.val());
                    ngModel.$render();
                });
            });
        }
    }
});
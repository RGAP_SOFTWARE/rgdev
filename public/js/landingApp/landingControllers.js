landingApp.controller('headerController', ['$scope', '$rootScope', function($scope, $rootScope){


    $rootScope.user = true;
    $rootScope.admin = false;
    $rootScope.csrfToken = $scope.csrf;

    $scope.switchView = function(event){
        event.preventDefault();

        $rootScope.user = !$rootScope.user;
        $rootScope.admin = !$rootScope.admin;
    };

    $rootScope.photoPathDetector = function(){
        if($rootScope.windowWidth >= 1200){
            $rootScope.photoPath = 'lg';
        }else if($rootScope.windowWidth < 1200 && $rootScope.windowWidth >= 978){
            $rootScope.photoPath = 'md';
        }else if($rootScope.windowWidth < 978 && $rootScope.windowWidth >= 768){
            $rootScope.photoPath = 'sm';
        } else {
            $rootScope.photoPath = 'xs';
        }
    };

    $rootScope.$watch('windowWidth', function(){
       $rootScope.photoPathDetector();
    });


}]);

landingApp.controller('landingController', ['$scope', 'landing', '$rootScope', '$window', function($scope, landing, $rootScope, $window){

    $scope.init = function(){

        $scope.landing = [];
        $scope.landingFile = '';
        $scope.infoblockFiles = {
            1:{},
            2:{}
        };
        $scope.infoblock1Img = {};
        $scope.infoblock2Img = {};
        $scope.creatingInfoblock = false;
        $scope.creatingInfoblock2 = false;
        $scope.landingImage = '';

        $scope.windowWidth = $window.innerWidth;
        $scope.windowHeight = $window.innerHeight;

        $scope.getLanding();

    };

    $scope.creatingModeSwitcher = function(){
        $scope.creatingInfoblock = !$scope.creatingInfoblock;
    };

    $scope.creatingModeSwitcher2 = function(){
        $scope.creatingInfoblock2 = !$scope.creatingInfoblock2;
    };

    $scope.getLanding = function(){
        landing.getLanding().then(function(res){
            $scope.landing = res;
            console.info(res);
        },function(er){
            console.log(er);
            $scope.getLanding();
        });
    };

    $scope.cleanCreateForm = function(){
      $scope.creatingModeSwitcher();
        $scope.newInfoblockText  = '';
        $scope.infoblockImg['new'] = '';
    };

    $scope.cleanCreateForm2 = function(){
        $scope.creatingModeSwitcher2();
        $scope.newInfoblock2Text  = '';
        $scope.infoblock2Img['new'] = '';
    };

    $scope.createInfoblock = function($data, infoblock){
        var data = {
            model: {
                info_text: $data || 'Some information'
            },
            file: $scope.infoblockFiles[infoblock]['new']
        };

        var token = $rootScope.csrfToken;

        landing.infoblock(data, token, 'create', infoblock).then(function(res){
            console.info(res);

            if(infoblock == 1)
                $scope.cleanCreateForm();
            else
                $scope.cleanCreateForm2();

            $scope.getLanding();
        },function(res){
            console.log(res);
        });
    };

    $scope.deleteInfoblock = function(infoblock, id){
        landing.deleteInfoblock(infoblock, id).then(function(res){
            console.info(res);
            $scope.getLanding();
        },function(res){
            console.log(res);
        });
    };

    $scope.editInfoblock = function($data, infoblock, infoblock_id, index){

        var data = {
            model: {
                info_text: $data,
                id: infoblock_id
            },
            file: $scope.infoblockFiles[infoblock][infoblock_id]
        };

        var token = $rootScope.csrfToken;

        landing.infoblock(data, token, 'edit', infoblock).then(function(res){
            console.info(res);
            $scope.getLanding();
        },function(res){
           console.log(res);
        });

    };

    $scope.editLanding = function(landingFile){

        var data = {
            model: {
                header_video: $scope.landing.data[0].header_video,
                footer_text: $scope.landing.data[0].footer_text,
                footer_contacts: $scope.landing.data[0].footer_contacts,
                footer_office: $scope.landing.data[0].footer_office
            },
            file: landingFile
        };

        var token = $rootScope.csrfToken;

        landing.editLanding(data, token).then(function(res){
            console.log(res);
            $scope.getLanding();
        }, function(res){
            console.log(res);
        });
    };

    $scope.prevent = function(event){
        event.preventDefault();
    };

    $scope.init();

}]);

landingApp.controller('footerController', ['$scope', function($scope){

}]);
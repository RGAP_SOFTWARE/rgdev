landingApp.service('landing', ['$http', '$q', function($http, $q){

    var landing = this;

    landing.getLanding = function(){
        var defer = $q.defer();

        $http.get('/landing/get').success(function(res){
            defer.resolve(res);
        }).error(function(er){
           defer.reject(er);
        });

        return defer.promise;
    };

    landing.deleteInfoblock = function(infoblock, id){
        var defer = $q.defer();

        $http.delete('/infoblock' + infoblock + '/delete/' + id).success(function(res){
            defer.resolve(res);
        }).error(function(res){
            defer.reject(res);
        });

        return defer.promise;
    };

    landing.infoblock = function(data, token, mode, infoblock){

        var defer = $q.defer();
        var FD = new FormData();

        for(var k in data.model){
            if(data.model.hasOwnProperty(k)){
                FD.append(k, data.model[k]);
            }
        }

        if(data.file){
            FD.append('file', data.file);
        }

        var url = '';

        if(mode == 'create'){
            url = '/infoblock' + infoblock + '/create';
        }else{
            url = '/infoblock' + infoblock + '/edit';
        }

        $http.post(url, FD, {
            transformRequest: angular.indentity,
            headers: { 'Content-Type': undefined, 'X-CSRF-TOKEN': token}
        }).success(function(res){
            defer.resolve(res);
        }).error(function(res){
            defer.reject(res);
        });

        return defer.promise;

    };

    landing.editLanding = function(data, token){

        var defer = $q.defer();
        var FD = new FormData();

        for(var k in data.model){
            if(data.model.hasOwnProperty(k)){
                FD.append(k, data.model[k]);
            }
        }

        if(data.file){
            FD.append('file', data.file);
        }

        $http.post('/landing/edit', FD,{
            transformRequest: angular.indentity,
            headers: { 'Content-Type': undefined, 'X-CSRF-TOKEN': token}
        }).success(function(res){
            defer.resolve(res);
        }).error(function(res){
           defer.reject(res);
        });

        return defer.promise;

    };

    return landing;

}]);
var landingApp = angular.module('landingApp', ['ngRoute',
                                                'ngResource',
                                                'ngAnimate',
                                                'ngTouch',
                                                'ngMessages',
                                                'ui.bootstrap',
                                                'xeditable'])
                .config(['$routeProvider', function($routeProvider){

                    $routeProvider.when('/',{

                        templateUrl: '/view/landingView/landing.htm',
                        controller: 'landingController'

                    }).otherwise({redirectTo: '/'});

                }]);
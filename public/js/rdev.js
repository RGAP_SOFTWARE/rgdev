var module = angular.module("rdevModule", ["ngAnimate"], ['$interpolateProvider', function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
	}]);

////////////////////////////////////////////////// main controller ////////////////////////////////////

module.controller("mainController", ['$scope', function ($scope) {

		$scope.overflowOffSwitch = false;

		$scope.flashMessage = true;

		$scope.closeFlash = function(){
			$scope.flashMessage = false;
		};

		/////////////////////////////// article delete alert modal //////////////////////////////////

    	$scope.articleId = '';

    	$scope.deleteModal = false;
        
    	$scope.deleteAlertModal = function($event,id){

    		if(typeof $event === 'object'){
    			$event.stopPropagation();
    			$event.preventDefault();
    		}

    		if(typeof $event !== 'object' && id === undefined)
    			$scope.articleId = $event;
    		else
    			$scope.articleId = id;

    		$scope.deleteModal = !$scope.deleteModal;

    		$scope.overflowOffSwitch = !$scope.overflowOffSwitch;
    		
    	}

    	//////////////////////////////////////////// searh validate ////////////////////////////////////

    	$scope.searchQ = '';
    	$scope.searchValid = '';

    	$scope.searchValidate = function($event){
    		if($scope.searchQ === undefined || $scope.searchQ === '' || $scope.searchQ === null){
    			$event.preventDefault();
    			$scope.searchValid = false;
    		}else{
    			$scope.searchValid = true;
    		}
    	}

    	//////////////////////////////////////////// edit modal ///////////////////////////////////////

    	$scope.editModal = false;

    	$scope.editArticleModal = function($event){

    		if(typeof $event === 'object'){
    			$event.stopPropagation();
    			$event.preventDefault();
    		}

    		$scope.editModal = !$scope.editModal;

    		$scope.overflowOffSwitch = !$scope.overflowOffSwitch;
    	}

    	

}]);	

////////////////////////////////////////////////////////////////////////////////
    module.controller("createFormController", ['$scope', function ($scope) {
                    
		$scope.createForm = {};

    }]);

    ////////////////////////////////////////////////////////////////////////////////
    module.controller("editFormController", ['$scope', '$http','$httpParamSerializerJQLike', function ($scope, $http, $httpParamSerializerJQLike) {

    	

    }]);

    module.controller("commentFormController", ['$scope', '$http','$httpParamSerializerJQLike', function ($scope, $http, $httpParamSerializerJQLike) {

        

    }]);

    //////////////////////////////////////////////// main menu controller ///////////////////////////////

	module.controller("mainMenuController", ['$scope', function ($scope) {

	    $scope.modalMenu = false;
	    $scope.buttonActive = "menu-button-active";

	    $scope.toggleMenu = function(element){
	       	$scope.modalMenu = !$scope.modalMenu;
	    }

	}]);    



	/////////////////////////////////// directives /////////////////////////////////

	module.directive('validFile',function(){
	  return {
	    require:'ngModel',
	    link:function(scope,el,attrs,ngModel){
	      //change event is fired when file is selected
	      el.bind('change',function(){
	        scope.$apply(function(){
	          ngModel.$setViewValue(el.val());
	          ngModel.$render();
	        });
	      });
	    }
	  }
	});

	module.directive('useFormData', function ($parse) {
	    return {
	      link: function (scope, element, attrs) {
	        if (attrs.ngModel && element[0].defaultValue) {
	          $parse(attrs.ngModel).assign(scope, element[0].defaultValue);
	        }
	      }
	    };
	});

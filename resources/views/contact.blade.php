<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $title }}</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href="/public/css/bootstrap.min.css" rel="stylesheet">
    <link href="/public/css/app.css" rel="stylesheet">

</head>
<body>

<div class="about-wrapper col-md-offset-1 col-md-10 col-lg-8 col-lg-offset-2 col-sm-12 col-xs-12">
    <div class="contacts">
        <a href="mailto:rgilyov@gmail.com" class="mail">
            rgilyov@gmail.com
        </a>
    </div>
</div>

</body>
</html>
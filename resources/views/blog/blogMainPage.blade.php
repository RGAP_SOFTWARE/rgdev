@extends('mainPage')

@section('header')

	<div class="rg-modal-wrapper ng-hide" ng-show="deleteModal">
		<div class="rg-modal">
			<div class="modal-header">
				<div class="rg-modal-close" ng-click="deleteAlertModal('')">
					<span class="glyphicon glyphicon-remove"></span>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="modal-body">
				<div class="text">Do you realy want to delete the article?</div>
			</div>
		    <div class="modal-footer">
		    	<div class="delete-button-block pull-right">
		    		<a href="/article/<% articleId %>/del" class="blog-button del-the-article">
						Delete
		    		</a>
		    	</div>
		    </div>
		</div>
		<div class="rg-overlay" ng-click="deleteAlertModal('')"></div>
	</div>

	@include('blog.blogHeaderContent')

	@if(Session::has('created_article'))
		<div class="alert alert-success flash-message" ng-if="flashMessage" ng-click="closeFlash()">{{ Session::get('created_article') }}</div>
	@endif

	@if(Session::has('deleted_article'))
		<div class="alert alert-danger flash-message" ng-if="flashMessage" ng-click="closeFlash()">{{ Session::get('deleted_article') }}</div>
	@endif
@stop

@section('content')
	@yield('blog-content')
@stop

@section('footer')
	@include('blog.blogFooterContent')
@stop

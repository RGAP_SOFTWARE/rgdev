@extends('blog.blogMainPage')


@section('blog-content')

	
	<h2>Create a new article</h2>

	@if ($errors->any())
		<ul class="alert alert-danger">
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	@endif

	<hr>

	{!! Form::open(['url' => 'articles', 'enctype' => 'multipart/form-data', 'name' => 'createForm', 'novalidate', 'ng-controller' => 'createFormController', 'class' => 'rg-form']) !!}

		<div class="form-group">
			<div class="required info ng-hide" ng-show="createForm.title.$error.required">Required field</div>
			<div class="required ng-hide" ng-show="createForm.title.$error.minlength && createForm.title.$dirty">Must be at least 3 simbols long</div>
			<div class="required ng-hide" ng-show="createForm.title.$error.maxlength && createForm.title.$dirty">Can't be more than 250 simbols</div>
			{!! Form::label('title', 'Title*:') !!}
			{!! Form::text('title', null, ['class' => 'form-control',
											'ng-model' => "title",
											'required',
											'ng-minlength' => '3',
											'ng-maxlength' => '250'
			]) !!}
		</div>

		<div class="form-group">
			<div class="required info ng-hide" ng-show="createForm.photo.$error.required">Required field</div>
			{!! Form::label('photo', 'Photo*:') !!}
			{!! Form::file('photo', ['class' => '',
									  'ng-model' => 'photo',
									  'required',
									  'valid-file'
			]) !!}
		</div>

		<div class="form-group">
			<div class="required info ng-hide" ng-show="createForm.body.$error.required">Required field</div>
			<div class="required ng-hide" ng-show="createForm.body.$error.minlength && createForm.body.$dirty">Must be at least 400 simbols long</div>
			{!! Form::label('body', 'Body*:') !!}
			{!! Form::textarea('body', null, ['class' => 'form-control',
												'ng-model' => 'body',
												'required',
												'ng-minlength' => '400'
			]) !!}
		</div>

		<div class="form-group">
			{!! Form::label('tags', 'Tags:') !!}
			{!! Form::select('tags[]', $tags, null, ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}
		</div>

		<div class="form-group">
			{!! Form::submit('Create an article', ['class' => 'btn btn-primary form-control',
													'ng-disabled' => 'createForm.$invalid'
			]) !!}
		</div>

	{!! Form::close() !!}



@stop


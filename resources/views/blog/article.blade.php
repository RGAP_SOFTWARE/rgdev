@extends('blog.blogMainPage')

@section('blog-content')

	@if($CurrentUser != null && $CurrentUser->id == $article->user_id)
		<div class="rg-modal-wrapper ng-hide" ng-show="editModal">
			<div class="rg-modal edit-article-modal">
				<div class="rg-modal-close" ng-click="editArticleModal()">
					<span class="glyphicon glyphicon-remove"></span>
				</div>
				@include('blog.editArticleForm')
			</div>
			<div class="rg-overlay" ng-click="editArticleModal()"></div>
		</div>
	@endif
	

	<article class="main-view">
		<div class="title">
			{{ $article->title }}
		</div>

		<div class="information-block description-block">

			<div class="article-date">
				<span class="sub-text">Published at: </span>
				{{ $article->published_at }}
			</div>


			<div class="author information-text">
				<span class="sub-text">By: </span>
				<a href="/articles/author/{{ $article->user->id }}">
					{{ $article->user->name }}
				</a>
			</div>

			@if($article->tags->toArray())
				<div class="tags-block">
					<span class="sub-text">Tags:</span>
					@foreach($article->tags->toArray() as $tag)
						<a class="tag" href="/articles/tag/{{ $tag['id'] }}">{{ $tag['name'] }}</a>
					@endforeach
				</div>
			@endif

		</div>

		@if($CurrentUser != null && $CurrentUser->id == $article->user_id)
			<div class="buttons-block">
				<div class="article-button">
					<a href="/article/{{ $article->id }}/edit" ng-click="editArticleModal($event);" class="rg-link edit-the-article blog-button rg-transition">Edit</a>
				</div>
				<div class="article-button">
					<a href="/article/{{ $article->id }}/del" ng-click="deleteAlertModal($event, {{ $article->id }})" class="rg-link del-the-article blog-button rg-transition">Delete</a>
				</div>
			</div>
			<div class="clearfix"></div>
		@endif
	
		<div class="article-image">
			<img class="img-responsive" src="/media/blog/article_images/{{ $article->images }}"/>
		</div>
		<div class="article-body">
			{!! nl2br($article->body) !!}
		</div>
	
	</article>
	
	<div class="comments-container col-md-8 col-md-offset-2">
		
			@foreach($comments as $comment)
				<div class="comment-block col-md-12">
					<div class="user-data col-md-3">
						<div class="user-name">{{ $comment->user()->get()->toArray()[0]['name'] }}</div>
					</div>
					<div class="comment-body col-md-9">
						<div class="date">{{ $comment->published_at }}</div>
						<div class="text">{{ nl2br($comment->body) }}</div>
					</div>
					@if($CurrentUser != null && $CurrentUser->id == $article->user_id)
						<div class="buttons-block comment-buttons">
							<div class="article-button">
								<a href="/comment/{{ $comment->id }}/edit" class="rg-link edit-the-article blog-button rg-transition">Edit</a>
							</div>
							<div class="article-button">
								<a href="/comment/{{ $comment->id }}/del" ng-click="deleteAlertModal($event, {{ $comment->id }})" class="rg-link del-the-article blog-button rg-transition">Delete</a>
							</div>
						</div>
					@endif
				</div>
			@endforeach
		

		@if($CurrentUser != null)
			<div class="left-comment-container">
				<div class="title">Write a comment</div>
				{!! Form::open(['url' => 'comment/create',  'name' => 'commentForm', 'novalidate', 'ng-controller' => 'commentFormController', 'class' => 'rg-form']) !!}

					<div class="form-group">
						<div class="required info ng-hide" ng-show="commentForm.body.$error.required">Required field</div>
						<div class="required ng-hide" ng-show="commentForm.body.$error.minlength && commentForm.body.$dirty">Must be at least 3 simbols long</div>
						<div class="required ng-hide" ng-show="commentForm.body.$error.maxlength && commentForm.body.$dirty">Can't be more than 250 simbols</div>
						{!! Form::label('body', 'Body*:') !!}
						{!! Form::textarea('body', null, ['class' => 'form-control',
														'ng-model' => "body",
														'required',
														'ng-minlength' => '3',
														'ng-maxlength' => '250'
						]) !!}
					</div>

					<input type="hidden" name="user_id" value="{{ $CurrentUser->id }}">
					<input type="hidden" name="article_id" value="{{ $article->id }}">

					<div class="form-group">
						{!! Form::submit('Create a comment', ['class' => 'btn btn-primary form-control',
																'ng-disabled' => 'commentForm.$invalid'
						]) !!}
					</div>

				{!! Form::close() !!}
			</div>
		@else
			<div class="alert alert-danger">Log in or register before write a comment</div>
		@endif
	</div>

@stop
		
@extends('blog.blogMainPage')

@section('blog-content')
	
	<h2>
		Edit: '{{ $article->title }}'
	</h2>
	
	@if ($errors->any())
		<ul class="alert alert-danger">
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	@endif

	<hr>

	@include('blog.editArticleForm')


@stop
{!! Form::model($article, ['method' => 'PATCH', 'action' => ['blogController@update', $article->id], 'ng-controller' => 'editFormController', 'name' => 'editForm', 'class' => "rg-form"]) !!}

		<div class="form-group">
			
			<div class="required info ng-hide" ng-show="editForm.title.$error.required">Required field</div>
			<div class="required ng-hide" ng-show="editForm.title.$error.minlength && editForm.title.$dirty">Must be at least 3 simbols long</div>
			<div class="required ng-hide" ng-show="editForm.title.$error.maxlength && editForm.title.$dirty">Can't be more than 250 simbols</div>
			{!! Form::label('title', 'Title*:') !!}
			{!! Form::text('title', null, ['class' => 'form-control',
											'ng-model' => "title",
											'required',
											'ng-minlength' => '3',
											'ng-maxlength' => '250',
											'use-form-data'								  		
			]) !!}
		</div>

		<div class="form-group">
	
			<div class="required info ng-hide" ng-show="editForm.body.$error.required">Required field</div>
			<div class="required ng-hide" ng-show="editForm.body.$error.minlength && editForm.body.$dirty">Must be at least 400 simbols long</div>
			{!! Form::label('body', 'Body*:') !!}
			{!! Form::textarea('body', null, ['class' => 'form-control',
												'ng-model' => 'body',
												'required',
												'ng-minlength' => '400',
												'use-form-data'
			]) !!}
		</div>

		<div class="form-group">
			{!! Form::label('tags', 'Tags:') !!}
			{!! Form::select('tags[]', \App\Tag::lists('name', 'id'), $article->tags()->lists('id')->toArray(), ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}
		</div>

		<div class="form-group">
			{!! Form::submit('Edit', ['class' => 'btn btn-primary form-control',
													'ng-disabled' => 'editForm.$invalid'
			]) !!}
		</div>

	{!! Form::close() !!}
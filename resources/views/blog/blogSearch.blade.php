@extends('blog.blogMainPage')

@section('blog-content')
	
		<h2>{{ $name }}</h2>
		{!! $searchError !!}

	<div class="articles-block">
		
		@foreach ($articles as $article)
			<article class="article col-md-12">
				<div class="col-md-6 article-image">
					<a class="rg-transition" href="/article/{{ $article->id }}">
						<img class="img-responsive" src="/media/blog/article_images/{{ $article->images }}"/>
					</a>
				</div>
				<div class="col-md-6">
					<a href="/article/{{ $article->id }}">
						<div class="rg-transition article-title">{{ $article->title }}</div>
					</a>
					<div class="description-block">
						<div class="article-date information-text"><span class="sub-text">Published at: </span>{{ $article->published_at }}</div>
						<div class="author information-text"><span class="sub-text">By: </span><a href="/articles/author/{{ $article->user->id }}">{{ $article->user->name }}</a></div>
						@if($article->tags->toArray())
							<div class="tags-block">
								<span class="sub-text">Tags:</span>
								@foreach($article->tags->toArray() as $tag)
									<a class="tag" href="/articles/tag/{{ $tag['id'] }}">{{ $tag['name'] }}</a>
								@endforeach
							</div>
						@endif
					</div>
					<div class="article-body">{!! nl2br($article->body) !!}</div>
					<div class="article-button">
						<a href="/article/{{ $article->id }}" class="rg-link read-more blog-button rg-transition">Keep reading</a>
					</div>
					@if($CurrentUser != null && $CurrentUser->id == $article->user_id)
						<div class="article-button">
							<a href="/article/{{ $article->id }}/edit" class="rg-link edit-the-article blog-button rg-transition">Edit</a>
						</div>
						<div class="article-button">
							<a href="/article/{{ $article->id }}/del" ng-click="deleteAlertModal($event, {{ $article->id }})" class="rg-link del-the-article blog-button rg-transition">Delete</a>
						</div>
					@endif
				</div>
				<div class="clear"></div>
				<hr>
			</article>
		@endforeach
	</div>

	<div class="pagination">
		{!! $articles->links() !!}
	</div>

@stop
<link rel="stylesheet" href="/css/select2.min.css">
<link rel="stylesheet" href="/css/blog.css">
	
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/blog">RGblog</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Topics <span class="caret"></span></a>
          <ul class="dropdown-menu">
            @foreach($tags as $tag)
				      <li><a href="/articles/tag/{{ $tag['id'] }}">{{ strtoupper($tag['name']) }}</a></li>
            @endforeach
            
          </ul>
        </li>
        <li><a href="/article/create">Create an article</a></li>
        <li><a href="/aboutme">About me</a></li>
        <li><a href="/contactme">Contact me</a></li>
      </ul>
      <a class="return-to-main-page navbar-brand navbar-right" title="To main page" href="/">
        <span class="glyphicon glyphicon-arrow-up rg-color" aria-hidden="true"></span>
      </a>
      <form class="navbar-form navbar-right" action="/articles/search" role="search">
        <div class="form-group relative-block search-block" ng-class="{'has-error': searchValid === false}">
          <input type="text" name="q" ng-model="searchQ" class="form-control" placeholder="Search">
          <button type="submit" ng-click="searchValidate($event)" class="btn btn-default rg-transition"><span class="glyphicon glyphicon-search"></span></button>
        </div>
      </form>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
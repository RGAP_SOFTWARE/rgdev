@extends('mainPage')

@section('header')

	<div class="goole-fonts menu-wrapper" ng-controller="mainMenuController">

		<span class="glyphicon glyphicon-menu-hamburger menu-button rg-transition" ng-class="{'menu-button-active': modalMenu === true}" ng-click="toggleMenu()"></span>
		<div class="modal-menu col-md-offset-2 col-md-4 rg-transition ng-hide" ng-show="modalMenu">
			<div class="menu-container col-md-10 col-md-offset-1">
				<a class="rglink link-container main-menu-link" href="/landing">
					<div class="col-md-12 menu-title rg-transition">Landing</div>
				</a>
				{{--<ul class='list-group'>--}}
					{{--<li class='list-group-item col-sm-4 col-md-12'><a class="rglink main-menu-link" href="/landing">User view</a></li>--}}
				{{--</ul>--}}
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="modal-menu col-md-4 rg-transition ng-hide" ng-show="modalMenu">
			<div class="menu-container col-md-10 col-md-offset-1">
				<a class="rglink link-container main-menu-link" href="/blog">
					<div class="col-md-12 menu-title rg-transition">Blog</div>
				</a>
				{{--<ul class='list-group'>--}}
					{{--<li class='list-group-item col-sm-4 col-md-12'><a class="rglink main-menu-link" href="/blog">User view</a></li>--}}
				{{--</ul>--}}
				<div class="clearfix"></div>
			</div>
		</div>
		{{--<div class="modal-menu col-md-4 rg-transition ng-hide" ng-show="modalMenu">--}}
			{{--<div class="menu-container col-md-10 col-md-offset-1">--}}
				{{--<div class="col-md-12 menu-title">Store:</div>--}}
				{{--<ul class='list-group'>--}}
					{{--<li class='list-group-item col-sm-4 col-md-12'><a class="rglink main-menu-link" href="/store">User view</a></li>--}}
					{{--<li class='list-group-item col-sm-4 col-md-12'><a class="rglink main-menu-link" href="/store/adminview">Admin view</a></li>--}}
					{{--<li class='list-group-item col-sm-4 col-md-12'><a class="rglink main-menu-link" href="/store/admin">Admin panel</a></li>--}}
				{{--</ul>--}}
				{{--<div class="clearfix"></div>--}}
			{{--</div>--}}
		{{--</div>--}}
		<div class="modal-menu col-md-4 col-md-offset-2 rg-transition ng-hide" ng-show="modalMenu">
			<div class="additional-menu-container menu-container col-md-10 col-md-offset-1">
				<ul class='list-group'>
					<li class='list-group-item'><a class="rglink additional-main-menu-link" href="/aboutme">About me</a></li>
					<li class='list-group-item'><a class="rglink additional-main-menu-link" href="/contactme">Contact me</a></li>
				</ul>
			</div>
		</div>
		<div class="modal-menu col-md-4 sm-padding rg-transition ng-hide" ng-show="modalMenu">
			<div class="menu-container col-md-10 col-md-offset-1">
				<div class="col-md-12 menu-tech-title">Technologies:</div>
				<ul class='list-group'>
					<li class='list-group-item tech-list-item col-md-12 col-sm-2 col-md-offset-0 col-sm-offset-1'>Laravel 5</li>
					<li class='list-group-item tech-list-item col-md-12 col-sm-2'>Angular js</li>
					<li class='list-group-item tech-list-item col-md-12 col-sm-2'>Twitter bootstrap 3</li>
					<li class='list-group-item tech-list-item col-md-12 col-sm-2'>MySql</li>
					<li class='list-group-item tech-list-item col-md-12 col-sm-2'>HTML5/CSS3</li>
				</ul>
				<div class="clearfix"></div>
				<span class="glyphicon glyphicon-menu-hamburger rg-transition menu-inner-button" ng-class="{'menu-button-active': modalMenu === true}" ng-click="toggleMenu()"></span>
			</div>
		</div>
		<div class="main-menu-overlay ng-hide" ng-show="modalMenu"></div>

	</div>

@stop


@section('content')
	<div class="google-fonts main-page-content col-md-12 container" ng-controller="mainPageController">
			<a href="/contactme" class="main-page-link">
		    	<div class="main-page-title col-xs-12">RGdev</div>
		    	<div class="main-page-text col-xs-12">There is nothing imposible</div>
			</a>
	</div>
@stop

@section('footer')
	<div class="goole-fonts col-xs-12 main-page-footer">
		Roman Gilyov
	</div>
@stop
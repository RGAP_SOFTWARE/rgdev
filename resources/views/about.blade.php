<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ $title }}</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link href="/public/css/bootstrap.min.css" rel="stylesheet">
        <link href="/public/css/app.css" rel="stylesheet">

    </head>
    <body>

        <div class="about-wrapper col-md-offset-1 col-md-10 col-lg-8 col-lg-offset-2 col-sm-12 col-xs-12">
            <div class="photo-block"></div>
            <div class="text">
                <pre>
                    Hi! I'm a full-stack web developer with more than 2 years commercial experience. And I love what I'm doing. If you want your app not just works fine but also easily maintained, has thoughtful, flexible structure and beautiful design, it means that you came on the right page and I'm definitely the guy that you need for creating or updating your app.

        My favorite stack of technologies:

        First it's - PHP that since 5.3 version can provide a numerous amount of flexibility and wonderful futures such: closures, type hinting, SPL library - that is a super power thing. And of course lately has been released 7th version of the language, that much, much more faster and have as well big number of cool stuff, that I want to use on your future app ;) And I can't forget about the most beautiful framework, in my opinion, that is Laravel that I love so much, my favorite tool to create a backend.

        Second thing is of course - javascript that is super strong language as well, especially AJAX technology that allows create single page applications, that is very, very cool for UX. And without delays, Angulrjs - it's what I'm prefer in the single page application development, because it gives a solid and absolutely magnificent structure for front-end. Code not just useful, but beautiful and clean as well that is just super.

        And third - a bunch of technologies I'm familiar with: html5, css3, SOUP, REST, jQuery, MySql, XML.

        So, I'm looking for new interesting challenges and places where I can write clear and thoughtful code. I always wait for your feedbacks. Thank you for reading!
                </pre>
            </div>
        </div>

    </body>
</html>
<!DOCTYPE html>
<html ng-app="blogApp">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>RG | Blog</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link href="/public/css/allblog.css" rel="stylesheet">
        <script src="/public/js/blogApp/all.js"></script>

    </head>
    <body>

        <div class="content-wrapper" ng-controller="rootController" window-size>
            <header  ng-controller="headerController">
                <input type="hidden" 
                        name="csrf"
                        use-default-value 
                        ng-model="csrf" 
                        value="{!! csrf_token() !!}">
                @include('blogApp.blogAppHeader')
            </header>

            <div class="notification alert alert-success pull-left col-md-12" ng-if="creatingSuccess" ng-click="hideNotifications()">
                New article has been successfully created
            </div>

            <div class="notification alert alert-success pull-left col-md-12" ng-if="deletingSuccess" ng-click="hideNotifications()">
                Your article has been successfully deleted
            </div>

            <div class="notification alert alert-success pull-left col-md-12" ng-if="updatingSuccess" ng-click="hideNotifications()">
                Your article has been successfully updated
            </div>

            <div class="notification alert alert-success pull-left col-md-12" ng-if="loginSuccess" ng-click="hideNotifications()">
                You have been successfully logged in
            </div>

            <div class="notification alert alert-success pull-left col-md-12" ng-if="logoutSuccess" ng-click="hideNotifications()">
                You have been successfully logged out
            </div>

            <div class="notification alert alert-success pull-left col-md-12" ng-if="registerSuccess" ng-click="hideNotifications()">
                You have been successfully registered
            </div>

            <div class="notification alert alert-danger pull-left col-md-12" ng-if="userNotExists" ng-click="hideNotifications()">
                There is no user registered with that email address.
            </div>

            <div class="clearfix"></div>
            
            <div class="container content">
                
               <div class="ng-view"></div>
            
            </div>
           
            <footer ng-controller="footerController">
                @include('blogApp.blogAppFooter')
            </footer>
            

        </div>

    </body>
</html>

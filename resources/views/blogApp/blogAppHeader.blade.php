
	
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#/">RGblog</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" ng-click="prevent($event)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tags <span class="caret"></span></a>
          <ul class="dropdown-menu">
            @foreach($tags as $tag)
				      <li><a href="#/tag/{{ $tag['id'] }}">{{ strtoupper($tag['name']) }}</a></li>
            @endforeach
            
          </ul>
        </li>
        <li><a href="#" ng-click="articleFormModal($event)">Create an article</a></li>
        <li><a href="/aboutme">About me</a></li>
        <li><a href="/contactme">Contact me</a></li>
      </ul>
      <a class="return-to-main-page navbar-brand navbar-right" title="To main page" href="/">
        <span class="glyphicon glyphicon-arrow-up rg-color" aria-hidden="true"></span>
      </a>
      <form class="navbar-form navbar-right" name="searchForm" ng-submit="search($event)" role="search">
        <div class="form-group relative-block search-block">
          <input type="text" name="q" ng-model="q" required class="form-control" placeholder="Search">
          <button type="submit" ng-disabled="searchForm.$invalid" class="btn btn-default rg-transition"><span class="glyphicon glyphicon-search"></span></button>
        </div>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li ng-if="authDetector"><a href="#" ng-click="logout($event)">LogOut</a></li>
        <li ng-if="!authDetector"><a href="#" ng-click="authModal($event)">Log in\Sign up</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
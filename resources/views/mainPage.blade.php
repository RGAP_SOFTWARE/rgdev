<!DOCTYPE html>
<html ng-app="rdevModule">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ $title }}</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <link href="/public/css/main.css" rel="stylesheet">
        <script src="/public/js/main.js"></script>

    </head>
    <body ng-controller="mainController" ng-class="{'overflowOff': overflowOffSwitch === true}">

        <div class="content-wrapper">
            <header>
                @yield('header')
            </header>
            
            <div class="container content">
                
                @yield('content')
            
            </div>
           
            <footer>
                @yield('footer')
            </footer>
            

        </div>

    </body>
</html>

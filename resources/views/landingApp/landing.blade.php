<!DOCTYPE html>
<html ng-app="landingApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RG | Landing</title>

    <link href="/public/css/alllanding.css" rel="stylesheet">
    <script src="/public/js/landingApp/all.js"></script>

</head>
<body>

<div class="content-wrapper">
    <header  ng-controller="headerController">
        <input type="hidden"
               name="csrf"
               use-default-value
               ng-model="csrf"
               value="{!! csrf_token() !!}">
        @include('landingApp.landingHeader')
    </header>

    <div class="clearfix"></div>

    <div>
        <div class="container-fluid content">

            <div class="ng-view" window-size></div>

        </div>
    </div>

    <footer ng-controller="footerController">
        @include('landingApp.landingFooter')
    </footer>


</div>

</body>
</html>

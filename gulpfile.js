var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */



elixir(function(mix) {
    mix.styles([
        'bootstrap.min.css',
        'app.css'
    ], 'public/css/main.css', 'public/css/').scripts([
        'old/angular.min.js',
        'old/angular-animate.min.js',
        'rdev.js',
        'jquery-2.2.0.min.js',
        'bootstrap.min.js'
    ], 'public/js/main.js', 'public/js/');
});

elixir(function(mix) {
    mix.styles([
        'bootstrap.min.css',
        'app.css',
        'landing.css'
    ], 'public/css/alllanding.css', 'public/css/')

    .scripts([
        'jquery-2.2.0.min.js',
        'bootstrap.min.js',
        'angular.js',
        'xeditable.min.js',
        'landingApp/landingApp.js',
        'landingApp/landingServices.js',
        'landingApp/landingControllers.js',
        'landingApp/landingDirectives.js',
        'landingApp/landingFilters.js'
    ], 'public/js/landingApp/all.js', 'public/js/');
});

elixir(function(mix) {
    mix.styles([
            'bootstrap.min.css',
            'select2.min.css',
            'app.css',
            'blog.css'
        ], 'public/css/allblog.css', 'public/css/')

        .scripts([
            'jquery-2.2.0.min.js',
            'bootstrap.min.js',
            'angular.js',
            'select2.min.js',
            'xeditable.min.js',
            'blogApp/blogApp.js',
            'blogApp/blogServices.js',
            'blogApp/blogControllers.js',
            'blogApp/blogDirectives.js',
            'blogApp/blogFilters.js'
        ], 'public/js/blogApp/all.js', 'public/js/');
});

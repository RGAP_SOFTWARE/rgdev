<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Landing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landing', function(Blueprint $table){
            $table->increments('id');
            $table->string('header_video');
            $table->string('footer_image');
            $table->string('footer_text');
            $table->string('footer_contacts');
            $table->string('footer_office');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('landing');
    }
}

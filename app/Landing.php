<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Landing extends Model
{
    protected $table = 'landing';

    public $timestamps = false;

    protected $fillable = [
        'header_video',
        'footer_image',
        'footer_text',
        'footer_contacts',
        'footer_office'
    ];
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposerServiseProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('blogApp.blogAppHeader', function($view){
            $tags = \App\Tag::all()->toArray();

            $view->with(['tags' => $tags]);
        });

        view()->composer('*', '\App\Http\Composers\userComposer');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

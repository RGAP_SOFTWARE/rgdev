<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class baseModel extends Model
{
    static public function resize($request, $path){
        $arr = ['xs/' => 768, 'sm/' => 978, 'md/' => 1200, 'lg/' => 2200];

        if($request->hasFile('file')){
            $fileName = str_random(10) . '.jpeg';

            foreach($arr as $k => $v){
                \Image::make($request->file('file'))
                    ->widen($v)
                    ->save($path . $k . $fileName);
            }

            return $fileName;
        }

        return false;
    }
}

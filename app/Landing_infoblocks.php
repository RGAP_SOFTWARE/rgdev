<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Landing_infoblocks extends Model
{
    protected $table = 'landing_infoblocks';

    public $timestamps = false;

    protected $fillable = [
        'info_text',
        'info_video'
    ];
}

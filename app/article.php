<?php

namespace App;


use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;

class article extends Model
{

    protected $table = "articles";
    
    protected $fillable = [
    	'title',
    	'body',
    	'published_at',
    	'images'
    ];

    public function setPublishedAtAttribute($date){
    	return $this->attributes['published_at'] = Carbon::createFromFormat('Y-m-d', $date);
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function tags(){
        return $this->belongsToMany('App\Tag')->withTimestamps();
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public function generateFileName($lenght){
        return str_random($lenght) . '.jpg';
    }

    static function sliceArticles($articles, $lenght = 400){
        $cnt = count($articles);
        for($i = 0; $i < $cnt; $i++){
            $articles[$i]->body = str_limit($articles[$i]->body, $lenght);
        }

        return $articles;
    }

    static function saveArticlePhoto($request){

        $arr = ['xs/' => 768, 'sm/' => 978, 'md/' => 1200, 'lg/' => 2200];

        if($request->hasFile('file_0')){
            $destinationPath = 'media/blog/';
            $fileName = str_random(10) . '.jpeg';

            foreach($arr as $k => $v){
                \Image::make($request->file('file_0'))
                                    ->widen($v)
                                    ->save($destinationPath . $k . $fileName);
            }

            return $fileName;
        }

        return false;

    }
}

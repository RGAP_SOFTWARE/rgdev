<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Landing_infoblocks2 extends Model
{
    protected $table = 'landing_infoblocks2';

    public $timestamps = false;

    protected $fillable = [
        'info_text',
        'info_video'
    ];
}

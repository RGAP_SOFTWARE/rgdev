<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'mainController@getMain');

	//-----------------------------------------------contact me routes----------------------------------------------//

	Route::get('/contactme', 'contactmeController@contact');
	Route::get('/aboutme', 'contactmeController@about');

	//-----------------------------------------------blog routes----------------------------------------------//

	 Route::get('/blog', 'BlogApiController@index');
	 Route::get('/get/user', 'BlogApiController@getCurrentUser');
	 Route::get('/blog/article/{id}', 'BlogApiController@article');
	 Route::get('/blog/article/comments/{id}', 'BlogApiController@comments');
	 Route::get('/blog/articles/{quantity}/{order}/{mode}/{query}/', 'BlogApiController@articles');
	 Route::post('/blog/create', 'BlogApiController@create');
	 Route::post('/blog/update', 'BlogApiController@update');
	 Route::post('/save/comment', 'BlogApiController@saveComment');
	 Route::post('/edit/comment', 'BlogApiController@editComment');
	 Route::delete('/blog/delete/{id}/{user_id}', 'BlogApiController@delete');
	 Route::delete('/delete/comment/{id}', 'BlogApiController@deleteComment');

	//------------------------------------------------ landing routes -------------------------------------------//

	Route::get('/landing', 'landingController@index');
	Route::get('/landing/get', 'landingController@getLanding');
	Route::post('/landing/edit', 'landingController@editLanding');
	Route::post('/infoblock1/edit', 'landingController@editInfoblock');
	Route::post('/infoblock2/edit', 'landingController@editInfoblock2');
	Route::post('/infoblock1/create', 'landingController@createInfoblock');
	Route::post('/infoblock2/create', 'landingController@createInfoblock2');
	Route::delete('/infoblock1/delete/{id}', 'landingController@deleteInfoblock');
	Route::delete('/infoblock2/delete/{id}', 'landingController@deleteInfoblock2');

	 
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
});

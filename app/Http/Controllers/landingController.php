<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\baseModel;
use App\Landing;
use App\Landing_infoblocks;
use App\Landing_infoblocks2;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class landingController extends Controller
{

    protected $footer_image_path = 'media/landing/';
    protected $infoblock_image_path = 'media/landing/infoblock/';
    protected $infoblock2_image_path = 'media/landing/infoblock2/';

    public function index(){
        return view('landingApp.landing');
    }

    public function getLanding(){

        $landing['data'] = Landing::all()->toArray();
        $landing['info1'] = Landing_infoblocks::all()->toArray();
        $landing['info2'] = Landing_infoblocks2::all()->toArray();

        return $landing;
    }

    public function editLanding(Requests\landingRequest $request){

        $landing = Landing::find(1);

        $landing->footer_image = baseModel::resize($request, $this->footer_image_path);

        if($landing->update($request->all())){
            return "true";
        }

        return 'false';
        

    }

    public function editInfoblock(Requests\infoblockRequest $request){

        $infoblock = Landing_infoblocks::find($request->input('id'));

        $infoblock->info_video = baseModel::resize($request, $this->infoblock_image_path);

        if($infoblock->update($request->all())){
            return "true";
        }

        return 'false';
        
    }

    public function editInfoblock2(Requests\infoblock2Request $request){

        $infoblock2 = Landing_infoblocks2::find($request->input('id'));

        $infoblock2->info_video = baseModel::resize($request, $this->infoblock2_image_path);

        $infoblock2->info_text = $request->info_text;

        if($infoblock2->update()){
            return "true";
        }

        return 'false';
        
    }

    public function createInfoblock(Requests\infoblockRequest $request){

        if(!$request->info_video = baseModel::resize($request, $this->infoblock_image_path))
            $request->info_video = '';

        $infoblock = new Landing_infoblocks($request->all());
        $infoblock->info_video = $request->info_video;

        if($infoblock->save()){
            return "true";
        }

        return 'false';
        
    }

    public function createInfoblock2(Requests\infoblock2Request $request){

        if(!$request->info_video = baseModel::resize($request, $this->infoblock2_image_path))
            $request->info_video = '';

        $infoblock2 = new Landing_infoblocks2($request->all());
        $infoblock2->info_video = $request->info_video;

        if($infoblock2->save()){
            return "true";
        }

        return 'false';
        
    }

    public function deleteInfoblock($id){

        if(Landing_infoblocks::destroy($id)){
           return 'true';
        }

        return 'false';
        
    }

    public function deleteInfoblock2($id){

        if(Landing_infoblocks2::destroy($id)){
            return 'true';
        }

        return 'false';

    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Intervention\Image\Facades\Image;
use Session;
use App\Comment;
use App\Tag;
use App\User;
use App\Article;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BlogApiController extends Controller
{

		/**
	     * On the index page
	     *
	     * @param  
	     * @return view
	    */

	    public function index(){

	    	return view('blogApp.blog');

	    }

        public function getCurrentUser(){

            $check = (Auth::check()) ? 'true' : 'false';

            return $check;
        }

        public function comments($id){

            $comments = Article::find($id)->comments()->get()->toArray();

            foreach($comments as $k => $v){
                $comments[$k]['user'] = User::find($v['user_id'])->toArray();
            }

            return $comments;

        }

        public function editComment(Requests\CommentRequest $request){

            if(Auth::user() !== null && $request->input('user_id') == Auth::user()->id){
                $comment = Comment::find($request->input('id'));
                $comment->timestamps = false;
                $comment->update(['body' => $request->input('body')]);
                return 'true';
            }

            return 'false';

        }

        public function saveComment(Requests\CommentRequest $request){

            $comment = new \App\Comment($request->all());

            $comment->timestamps = false;

            if($comment->save())
                return 'true';
            else
                return 'false';

        }

        public function deleteComment($id){

            $comment = Comment::find($id);

            if(Auth::user() !== null && Auth::user()->id == $comment->user_id){
                Comment::destroy($id);
                return 'true';
            }

            return 'false';

        }

        public function article($id){

            $article = Article::find($id);
            $data['article'] = $article->toArray();
            $data['article']['userName'] = $article->user()->get()[0]['name'];
            $data['article']['tags'] = $article->tags()->get()->toArray();
            $data['article']['currentUser'] = Auth::user();
            $data['tags'] = Tag::all()->toArray();

            return $data;

        }

    	/**
         * Get articles from db
         *
         * @param  integer, string
         * @return json
        */

        public function articles($quantity, $order, $mode, $query, Request $request){

            if($mode === 'firstTime'){
                if(Session::get('order')){
                    $quantity = Session::get('quantity');
                    $order = Session::get('order');
                }
            }else{
                Session::put([
                    'quantity' => $quantity,
                    'order' => $order
                ]);
            }

            switch($mode){
                case 'search':
                   $articles = Article::orderBy('id', $order)
                                ->where('title', 'LIKE', '%' . $query . '%')
                                ->paginate($quantity);
                   $subTitle = ucfirst($query) . " articles:";
                break;

                case 'author':
                   $articles = User::find($query)
                                ->articles()
                                ->orderBy('id', $order)
                                ->paginate($quantity);
                   $subTitle = User::find($query)->name . '\'s articles:';
                break;

                case 'tag':
                   $articles = Tag::find($query)
                                ->articles()
                                ->orderBy('id', $order)
                                ->paginate($quantity);
                   $subTitle = ucfirst(Tag::find($query)->name) . ' articles:';
                break;

                default:
                   $subTitle = '';
                    $articles = Article::orderBy('id', $order)->paginate($quantity);
                break;
            }

        	$data = $articles->toArray();
            $data['subTitle'] = $subTitle;

        	$cnt = count($data['data']);
        	for($i = 0; $i < $cnt; $i++){
	            $data['data'][$i]['userName'] = User::find($data['data'][$i]['user_id'])->name;
	            $data['data'][$i]['tags'] = Article::find($data['data'][$i]['id'])->tags()->get()->toArray();
	        }

            $data['tags'] = Tag::all()->toArray();
            $data['currentUser'] = Auth::user();

            if($order === 'desc')
                $data['title'] = 'Latest articles.';
            else
                $data['title'] = 'Oldest articles.';

            $data['order'] = $order;

        	return $data;
        }

    	/**
         * create a new article
         *
         * @param  
         * @return 
        */    

        public function create(Requests\ArticleRequest $request){

            if(Auth::user() !== null){

                $article = new Article($request->all());

                $article->images = Article::saveArticlePhoto($request);

                Auth::user()->articles()->save($article);

                $tags = explode(',', $request->input('tags'));
                $article->tags()->attach($tags);

                return "true";

            }else{

                return 'false';

            }
        }

        /**
         * update the article
         *
         * @param  
         * @return 
        */

        public function update(Requests\ArticleUpdateRequest $request){

            $user_id = $request->input('user_id') * 1;
            $id = $request->input('id');
            $input = $request->except(['user_id', 'id']);

            if(Auth::user() !== null && Auth::user()->id === $user_id){
                $article = Article::findOrFail($id);

                if($request->hasFile('file_0') && $request->file('file_0')->isValid()){
                    $imgName = $article->images;
                    \File::delete('media/blog/article_images/' . $imgName);
                    $res = Article::saveArticlePhoto($request);
                    if($res !== false)
                        $input['images'] = $res;
                }


                $article->update($input);
                $tags = explode(',', $request->input('tags'));
                $article->tags()->sync($tags);

                return 'true';
            }

            return 'false';

        }

    	/**
         * delete an article
         *
         * @param  id of article, user id
         * @return 
        */

        public function delete($id, $user_id){

        	if(Auth::user()->id == $user_id && Auth::user() !== null){
        		$article = Article::find($id);

        		\File::delete('media/blog/article_images/' . $article->images);

        		Article::destroy($id);

        		return 'true';

        	}else{
        		return 'false';
        	}

        }
}

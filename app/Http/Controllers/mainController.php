<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class mainController extends Controller
{
    public function getMain(){
    	$title = "RGdev";

    	return view('pageContent.mainContent')->with([
    		"title" => "$title"
    	]);
    }
}

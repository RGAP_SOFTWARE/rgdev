<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class contactmeController extends Controller
{
    public function contact(){

    	return view('contact')->with(['title' => 'Contact me']);

    }

    public function about(){
        return view('about')->with(['title' => 'About me']);
    }
}

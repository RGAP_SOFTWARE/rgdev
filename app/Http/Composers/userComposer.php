<?php 

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;

class userComposer{

	public function compose(view $view){

		return $view->with('CurrentUser', \Auth::user());

	}

}
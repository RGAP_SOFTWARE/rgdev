<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "comments";

    protected $fillable = [
    	'body',
    	'published_at',
    	'article_id',
    	'user_id'
    ];

    public function setPublishedAtAttribute($date){
    	return $this->attributes['published_at'] = Carbon::createFromFormat('Y-m-d', $date);
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function article(){
    	return $this->belongsTo('App\article');
    }
}
